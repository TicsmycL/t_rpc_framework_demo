package fun.ticsmyc.test.rpc.api;

import fun.ticsmyc.rpc.server.annotation.TRPCInterface;

/**
 * @author Ticsmyc
 * @date 2020-10-23 17:32
 */
@TRPCInterface
public interface ByeService {

    String goodBye(String name);

}
