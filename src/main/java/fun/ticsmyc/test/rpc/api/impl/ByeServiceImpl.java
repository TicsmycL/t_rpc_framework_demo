package fun.ticsmyc.test.rpc.api.impl;


import fun.ticsmyc.rpc.server.annotation.TRPCService;
import fun.ticsmyc.test.rpc.api.ByeService;

import java.io.Serializable;

/**
 * @author Ticsmyc
 * @date 2020-10-23 17:32
 */
@TRPCService(group = "t")
public class ByeServiceImpl implements Serializable,ByeService {
    @Override
    public String goodBye(String name) {
        return "再见，"+name;
    }
}
