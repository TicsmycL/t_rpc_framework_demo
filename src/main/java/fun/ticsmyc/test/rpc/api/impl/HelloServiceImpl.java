package fun.ticsmyc.test.rpc.api.impl;


import fun.ticsmyc.test.rpc.api.HelloService;
import fun.ticsmyc.rpc.server.annotation.TRPCService;

import java.io.Serializable;

/**
 * @author Ticsmyc
 * @date 2020-10-23 10:17
 */
@TRPCService(group="t")
public class HelloServiceImpl implements Serializable,HelloService {
    @Override
    public String hello(String name) {
        return "你好，"+name;
    }
}
