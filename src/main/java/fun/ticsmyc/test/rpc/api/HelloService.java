package fun.ticsmyc.test.rpc.api;

import fun.ticsmyc.rpc.server.annotation.TRPCInterface;

/**
 * @author Ticsmyc
 * @date 2020-10-23 9:59
 */
@TRPCInterface
public interface HelloService {

    String hello(String name);
}
