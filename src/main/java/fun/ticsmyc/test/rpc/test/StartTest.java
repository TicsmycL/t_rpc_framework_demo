package fun.ticsmyc.test.rpc.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * 同时启动服务端和客户端
 * @author Ticsmyc
 * @date 2020-11-23 15:51
 */
public class StartTest {

    public static void main(String[] args) throws InterruptedException {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfig.class);
        NettyTest bean = applicationContext.getBean(NettyTest.class);

        while(true){
            long startTime =System.currentTimeMillis();
            bean.test();
            long endTime = System.currentTimeMillis();
            System.out.println("用时:"+(endTime-startTime) +" ms");
            TimeUnit.SECONDS.sleep(2);
        }
    }
}
