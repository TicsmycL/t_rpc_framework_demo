package fun.ticsmyc.test.rpc.test;

import fun.ticsmyc.rpc.client.annotation.RpcClient;
import fun.ticsmyc.test.rpc.api.ByeService;
import fun.ticsmyc.test.rpc.api.HelloService;
import org.springframework.stereotype.Component;


/**
 * @author Ticsmyc
 * @date 2020-10-26 12:01
 */
@Component
public class NettyTest {

    @RpcClient(group = "t")
    private HelloService helloService;

    @RpcClient(group = "t")
    private ByeService byeService;

    public void test() {
        System.out.println(helloService.hello("苟利国家生死以"));
        System.out.println(byeService.goodBye("岂因祸福避趋之"));
    }
}
