package fun.ticsmyc.test.rpc.test;

import fun.ticsmyc.rpc.EnableTRPC;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Ticsmyc
 * @date 2020-11-23 16:57
 */
@ComponentScan({"fun.ticsmyc.test"})
@EnableTRPC
public class MyConfig {
}
